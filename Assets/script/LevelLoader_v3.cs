using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LevelLoader_v3 : MonoBehaviour 
{
	public static bool LEVEL_HAS_GUN = false;
	public static bool GUN_ACQUIRED = false;
	
	public static bool LEVEL_HAS_BUTTON = false;
	public bool BUTTON_PRESSED = false;
	
	public Transform endTile;
	public Transform gunTile;
	
	private int blockCount; // This may be necessary as public. I dont know.
	
	public List<Transform> LoadFromFile(string path, bool isEditMode)
	{	
		List<Transform> blocks = new List<Transform>();
		
		using(StreamReader reader = new StreamReader(path))
		{
			// Skipping header.
			for(int i=0; i<10; i++)
				reader.ReadLine();
			
			string line;
			line = reader.ReadLine();
			blockCount = int.Parse(line);
			
			// Each line contains data for a single cube.
			for(;(line = reader.ReadLine()) != null;)
			{
				Light[] lights;
				
				string[] words = line.Split(' ');
				
				float xPos = float.Parse(words[0]);
				float yPos = float.Parse(words[1]);
				float zPos = float.Parse(words[2]);
				int rot = int.Parse(words[3]);
				string type = words[4];
					
				int layer = int.Parse(words[5]);
				
				Transform newBlock;
				if(isEditMode)
				{
					newBlock = InstantiateEditBlock(new Vector3(xPos,yPos,zPos), Quaternion.Euler(0,rot*90,0), TypeToInteger(type), layer, 1);
				}
				else
				{
					newBlock = InstantiateBlock(new Vector3(xPos,yPos,zPos), Quaternion.Euler(0,rot*90,0), TypeToInteger(type), layer);
				}
				blocks.Add(newBlock);
				
				// Special treatments for blocks.
				// These have to be done manually.
				if(type == "BendTile")
				{
					newBlock.tag = "bender";
					
					//newBlock.gameObject.GetComponent<ParticleSystem>().enableEmission = true;
					
					lights = newBlock.gameObject.GetComponentsInChildren<Light>();
	    			foreach (Light l in lights)
					{
	    				l.enabled = true;
						l.color = new Color(1f, 0.5f, 0.5f);
					}
				}
				else if(type == "LightTile")
				{
					lights = newBlock.gameObject.GetComponentsInChildren<Light>();
	    			foreach (Light l in lights)
					{
	    				l.enabled = true;
					}
				}

				else if(type == "ButtonTile")
				{
					lights = newBlock.gameObject.GetComponentsInChildren<Light>();
					foreach (Light l in lights)
					{
						l.enabled = true;
						l.color = new Color(0.5f, 0.5f, 1f);
					}
					
					LEVEL_HAS_BUTTON = true;
				}
				
				else if(type == "Gun")
				{
					gunTile = newBlock;
					LEVEL_HAS_GUN = true;
				}
				else if(type == "StartTile")
				{
					if(!isEditMode)
						GameObject.Find("First Person Controller").GetComponent<PlayerScript>().setStartPosition(newBlock);
				}
				else if(type == "EndTile")
				{
					endTile = newBlock;
					
					lights = newBlock.gameObject.GetComponentsInChildren<Light>();
					foreach (Light l in lights)
					{
						l.enabled = true;
						l.color = new Color(0.5f, 0.5f, 1f);
					}
				}
			}
		}
		if(!LEVEL_HAS_BUTTON && !isEditMode)
		{
			GetComponent<MapScript>().PressButton();
		}
		if(!LEVEL_HAS_GUN)
		{
			GUN_ACQUIRED = true;
		}
		return blocks;
	}
	
	public Transform InstantiateBlock(Vector3 pos, Quaternion rot, int type, int layer)
	{
		Transform block;
		block = Instantiate(BlockScript.BlockPrefabs[type], pos, rot) as Transform;
		block.GetComponent<BlockInfo>().type = type;
		block.GetComponent<BlockInfo>().layer = layer;
		block.GetComponent<BlockInfo>().rotation = (int)rot.eulerAngles.y / 90;
		block.GetComponent<BlockInfo>().updateInfo();
				
		return block;
	}
	
	public Transform InstantiateEditBlock(Vector3 pos, Quaternion rot, int type, int layer, int currLayer)
	{
		Transform block;
		block = InstantiateBlock(pos,rot,type,layer);
		
		string name = "Layer " + layer.ToString();
		block.parent = GameObject.Find(name).transform;
		
		return block;
	}
	
	private int TypeToInteger(string typeName)
	{
		for(int i = 0; i<BlockScript.BlockNames.Length; i++)
		{
			if(typeName == BlockScript.BlockNames[i])
				return i;
		}
		
		Debug.LogError("Undefined block type.");
		return -1; // TODO: handle this
	}
}
