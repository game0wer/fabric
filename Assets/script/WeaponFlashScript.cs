using UnityEngine;
using System.Collections;
using System;

public class WeaponFlashScript : MonoBehaviour
{
	public GameObject GunCamera;
	private float counter = 0;
	
	private const float timeout = 0.4f;
	
	public void Flash()
	{
		counter = 0;
		audio.Play();
	}
	
	void Start()
	{
		counter = timeout;
		transform.localScale = new Vector3(0f,0f,0f);
	}

	void Update()
	{
		if (counter > timeout)
		{
			transform.localScale = new Vector3(0f,0f,0f);
			return;
		}
		counter += Time.deltaTime;
		transform.RotateAround(transform.up, Time.deltaTime*10.0f);
		float t = counter/timeout;
		GunCamera.transform.localPosition = new Vector3(0,0,1-t);
		GunCamera.transform.localEulerAngles = new Vector3((1-t)*20f,0,0);
		t = -1+2*t;
		t = 1-t*t;
		t *= 2f;
		transform.localScale = new Vector3(t,t,t);
	}
}
