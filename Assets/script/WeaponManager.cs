using UnityEngine;
using System.Collections;
using System;

public class WeaponManager : MonoBehaviour
{
	public GameObject globalScript;
	public GameObject weaponFlash;
	public GameObject bendGuides;
	public GameObject weaponCamera;
	
	private Transform firstBender = null;

	void Update ()
	{
		if (LevelLoader_v3.GUN_ACQUIRED)
		{
			weaponCamera.active = true;
		} else
		{
			weaponCamera.active = false;
			return;
		}
		if (Input.GetMouseButtonDown(1))
		{
			bendGuides.SetActiveRecursively(false);
			firstBender = null;
			
			if (globalScript.GetComponent<MapScript>().RevertMap())
				audio.Play();
			
		}
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			int layerMask = ~(1<<10);
			if (Physics.Raycast(transform.position, transform.forward, out hit, float.PositiveInfinity, layerMask))
			{
				if (hit.transform.tag == "bender" || hit.transform.tag == "black")
				{
					print("hit bender");
					weaponFlash.GetComponent<WeaponFlashScript>().Flash();
					if (firstBender == null)
					{
						firstBender = hit.transform;
						bendGuides.SetActiveRecursively(true);
						bendGuides.transform.position = hit.transform.position;
					}
					else if (hit.transform != firstBender.transform
							&& !IsAlreadyBent(hit.transform, firstBender.transform))
					{
						bendGuides.SetActiveRecursively(false);
						
						float xa = firstBender.transform.position.x;
						float ya = firstBender.transform.position.y;
						float za = firstBender.transform.position.z;
						int ta = firstBender.transform.gameObject.GetComponent<BlockInfo>().layer;
					
						float xb = hit.transform.position.x;
						float yb = hit.transform.position.y;
						float zb = hit.transform.position.z;
						int tb = hit.transform.gameObject.GetComponent<BlockInfo>().layer;
						
						if (firstBender.transform.tag == "black")
						{
							ta = -1;
						}
						if (hit.transform.tag == "black")
						{
							tb = -1;
						}
						
						bool succ = false;
						if (Math.Abs(ya-yb) < float.Epsilon && Math.Abs(za-zb) < float.Epsilon)
						{
							succ = globalScript.GetComponent<MapScript>().ShiftX(Math.Min(xa,xb), Math.Max(xa,xb),ta,tb);
						}
						else if (Math.Abs(za-zb) < float.Epsilon && Math.Abs(za-zb) < float.Epsilon)
						{
							succ = globalScript.GetComponent<MapScript>().ShiftY(Math.Min(ya,yb), Math.Max(ya,yb),ta,tb);
						}
						else if (Math.Abs(ya-yb) < float.Epsilon && Math.Abs(ya-yb) < float.Epsilon)
						{
							succ = globalScript.GetComponent<MapScript>().ShiftZ(Math.Min(za,zb), Math.Max(za,zb),ta,tb);
						}
						
						if (succ)
							audio.Play();
						
						
						firstBender = null;
						
					} else
					{
						Debug.Log("same or too close");
					}
				} else if (hit.transform.tag == "buttoner")
				{
					Vector3 buttonpos = hit.transform.position;
					buttonpos -= transform.position;
					if (buttonpos.magnitude < 3 && globalScript.GetComponent<LevelLoader>().PressButton())
					{
						Component[] lights = (hit.transform.gameObject).GetComponentsInChildren<Light>();
		    			foreach (Light l in lights)
						{
			        		l.enabled = false;
						}
						hit.transform.gameObject.GetComponent<VoxelScript>().disableLights = true;
						globalScript.GetComponent<LevelLoader>().PressButton();
					}
				}
			}
		}
	}
	
	private bool IsAlreadyBent(Transform arg1, Transform arg2)
	{
		// Edge of blocks are assumed to be 1 unit.
		return ((arg1.position - arg2.position).magnitude < 1.01);
	}
}
