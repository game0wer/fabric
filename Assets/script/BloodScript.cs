using UnityEngine;
using System.Collections;
using System;

public class BloodScript : MonoBehaviour
{
	public float Damage = 0;
	public const float HitDamage = 0.2f;
	
	public void Hit(bool falling = false)
	{
		Damage += HitDamage;
		if (!falling)
			audio.Play();
	}
	
	public void kill()
	{
		Application.LoadLevel("DeathScene");
	}
	
	void Update()
	{
		if (Damage<0)
			Damage = 0;
		else
			Damage -= Time.deltaTime/10;
		
		if (Damage>0.8f)
			kill();
			
		if (Damage>0)
			guiTexture.color = new Color(0.5f,0f,0f,Damage);
		else
			guiTexture.color = new Color(0.5f,0f,0f,0);
	}
}