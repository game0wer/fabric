using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using AssemblyCSharp;

public class LevelLoader : MonoBehaviour
{
	public Transform VoxelPrefab;
	public Transform VoxelLightPrefab;
	public Transform BendZonePrefab;
	public Transform Player;
	public Transform TurretPrefab;
	public Transform GunPrefab;
	public Transform EndPrefab;
	
	public Material GreyTileMaterial;
	public Material GreyTileAltMaterial;
	public Material GreyTileDoubleMaterial;
	public Material GreyTileQuadMaterial;
	public Material BloodyTileMaterial;
	public Material CrackedTileMaterial;
	public Material LightTileMaterial;
	public Material BendTileMaterial;
	public Material StartTileMaterial;
	public Material HelpTileMaterial;
	public Material ButtonTileMaterial;
	
	public Transform FPSCamera;
	
	private const int MAP_WIDTH = 32;
	private const int MAP_HEIGHT = 16;
	private const int MAP_DEPTH = 32;
	
	public static int LEVEL_NUM = 5;
	public static int LAST_LEVEL = 10;
	
	private bool LEVEL_HAS_BUTTON = false;
	private bool BUTTON_PRESSED = false;
	
	private bool LEVEL_HAS_GUN = false;
	public static bool GUN_AQUIRED = false;
	
	// are we playing user levels
	public static bool USER_MODE = false;
	
	
	public enum Tiles : byte { EMPTY = 0, GREY, BLOOD, CRACK, LIGHT, END, HELP, START, BEND, GUN, TURRET, ALT, DOUBLE, QUAD, BUTTON };
	
	private List<Object> voxels;
	
	private Transform endTile;
	
	void Start()
	{
		
		voxels = new List<Object>();
		
		LoadMapFromFile();
		
		if (LEVEL_HAS_GUN)
			GUN_AQUIRED = false;
		else 
			GUN_AQUIRED = true;
		
		if (LEVEL_HAS_BUTTON)
		{
			BUTTON_PRESSED = false;
		} else 
		{
			PressButton();
		}
		
		Debug.Log(LEVEL_NUM);
		Screen.lockCursor = true;
		Screen.showCursor = false;
	}
	
	
	public bool RevertMap()
	{
		bool ret = false;
		foreach(Object vx in voxels)
		{
			if (vx is Transform)
			{
				ret = ((Transform)vx).gameObject.GetComponent<VoxelScript>().Revert();
			} else if (vx is GameObject)
			{
				ret = ((GameObject)vx).GetComponent<VoxelScript>().Revert();
			} else
			{
				Debug.Log("OOPS");
			}
		}
		return ret;
	}
	public bool PressButton()
	{
		if (BUTTON_PRESSED)
			return false;
		
		BUTTON_PRESSED = true;

		for (int l = 1; l<5; l++)
		{
			Object nv = Instantiate(EndPrefab,
						 new Vector3(endTile.position.x,endTile.position.y+l,endTile.position.z),
						 Quaternion.identity);
			((Transform)nv).transform.localEulerAngles = new Vector3(-90,0,0);
			((Transform)nv).transform.parent = endTile;
			// only do it for the first
			if (l == 1)
			{
				((Transform)nv).transform.tag = "ender";
			}
			((Transform)nv).collider.isTrigger = true;
			
		}
		return true;
	}
	public bool ShiftX(float lower, float upper, byte typeA = 0, byte typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(1,0,0), new Vector3(lower+eps,0,0));
		Plane hi = new Plane(new Vector3(1,0,0), new Vector3(upper-eps,0,0));
		
		foreach(Object vx in voxels)
		{
			Transform xfrm = null;
			VoxelScript vs = null;
			GameObject go = null;
			if (vx is Transform)
			{
				xfrm = (Transform)vx;
				vs = ((Transform)vx).gameObject.GetComponent<VoxelScript>();
				go = ((Transform)vx).gameObject;
			} else if (vx is GameObject)
			{
				xfrm = ((GameObject)vx).transform;
				vs = ((GameObject)vx).GetComponent<VoxelScript>();
				go = (GameObject)vx;
			} else
			{
				Debug.Log("OOPS");
			}
			if (!low.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x-(lower - upper + 1)*0.5f, xfrm.position.y, xfrm.position.z),
								go.active));
				vs.flicker = false;
				
			} else if (hi.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x+(lower - upper + 1)*0.5f, xfrm.position.y, xfrm.position.z),
								go.active));
				vs.flicker = false;
			} else
			{
				vs.Apply(new VoxelState(new Vector3(0f, xfrm.localScale.y, xfrm.localScale.z),
								new Vector3((lower + upper)*0.5f, xfrm.position.y, xfrm.position.z),
								false));
				vs.flicker = true;
			}
		}
		return true;
	}
	public bool ShiftY(float lower, float upper, byte typeA = 0, byte typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(0,1,0), new Vector3(0,lower+eps,0));
		Plane hi = new Plane(new Vector3(0,1,0), new Vector3(0,upper-eps,0));
		
		foreach(Object vx in voxels)
		{
			Transform xfrm = null;
			VoxelScript vs = null;
			GameObject go = null;
			if (vx is Transform)
			{
				xfrm = (Transform)vx;
				vs = ((Transform)vx).gameObject.GetComponent<VoxelScript>();
				go = ((Transform)vx).gameObject;
			} else if (vx is GameObject)
			{
				xfrm = ((GameObject)vx).transform;
				vs = ((GameObject)vx).GetComponent<VoxelScript>();
				go = (GameObject)vx;
			} else
			{
				Debug.Log("OOPS");
			}
			if (!low.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y-(lower - upper + 1)*0.5f, xfrm.position.z),
								go.active));
				vs.flicker = false;
				
			} else if (hi.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y+(lower - upper + 1)*0.5f, xfrm.position.z),
								go.active));
				vs.flicker = false;
			} else
			{
				vs.Apply(new VoxelState(new Vector3(xfrm.localScale.x, 0f, xfrm.localScale.z),
								new Vector3(xfrm.position.x, (lower + upper)*0.5f, xfrm.position.z),
								false));
				vs.flicker = true;
			}
		}
		return true;
	}
	
	public bool ShiftZ(float lower, float upper, byte typeA = 0, byte typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(0,0,1), new Vector3(0,0,lower+eps));
		Plane hi = new Plane(new Vector3(0,0,1), new Vector3(0,0,upper-eps));
		
		foreach(Object vx in voxels)
		{
			Transform xfrm = null;
			VoxelScript vs = null;
			GameObject go = null;
			if (vx is Transform)
			{
				xfrm = (Transform)vx;
				vs = ((Transform)vx).gameObject.GetComponent<VoxelScript>();
				go = ((Transform)vx).gameObject;
			} else if (vx is GameObject)
			{
				xfrm = ((GameObject)vx).transform;
				vs = ((GameObject)vx).GetComponent<VoxelScript>();
				go = (GameObject)vx;
			} else
			{
				Debug.Log("OOPS");
			}
			if (!low.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y, xfrm.position.z-(lower - upper + 1)*0.5f),
								go.active));
				vs.flicker = false;
				
			} else if (hi.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y, xfrm.position.z+(lower - upper + 1)*0.5f),
								go.active));
				vs.flicker = false;
			} else
			{
				vs.Apply(new VoxelState(new Vector3(xfrm.localScale.x, xfrm.localScale.y, 0f),
								new Vector3(xfrm.position.x, xfrm.position.y, (lower + upper)*0.5f),
								false));
				vs.flicker = true;
			}
		}
		return true;
	}
	
	void LoadMapFromFile()
	{
		StreamReader sr;
		if (USER_MODE)
		{
			sr = new StreamReader(Application.dataPath + "/user_levels/level"+LEVEL_NUM+".cko");
		} else
		{
			TextAsset levelfile = Resources.Load("levels/level" + LEVEL_NUM) as TextAsset;
			MemoryStream ms = new MemoryStream(levelfile.bytes);
			sr = new StreamReader(ms);
		}
		// skip header
		sr.ReadLine();
		sr.ReadLine();
		
		// read num voxels
		int numVox = int.Parse(sr.ReadLine());
		
		// skip marker voxels
		sr.ReadLine();
		sr.ReadLine();
		sr.ReadLine();
		
		// init level
		for (int i = 0; i<numVox-1; i++)
		{
			string lnstr = sr.ReadLine();
			if(lnstr == null)
				break;
			string[] line = lnstr.Split(' ');
			int vx = int.Parse(line[0])-100;
			int vy = int.Parse(line[1])-100;
			int vz = int.Parse(line[2])-100;
			string color = line[3];
			
			Object nv;
			Light[] lights;
			
			// lights
			if (color == "ffff0000") 		// light
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((Transform)nv).gameObject.renderer.material = LightTileMaterial;
				lights = ((Transform)nv).gameObject.GetComponentsInChildren<Light>();
    			foreach (Light l in lights)
				{
    				l.enabled = true;
				}
				
			} else if (color == "ff00ff00")	// start
			{
				Player.transform.position = new Vector3(vx, vy+2, vz);
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = GreyTileMaterial;
				
			} else if (color == "ff0000ff")	// bend
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((Transform)nv).tag = "bender";
				((Transform)nv).gameObject.GetComponent<ParticleSystem>().enableEmission = true;
				((Transform)nv).gameObject.renderer.material = BendTileMaterial;
				lights = ((Transform)nv).gameObject.GetComponentsInChildren<Light>();
    			foreach (Light l in lights)
				{
    				l.enabled = true;
					l.color = new Color(1f, 0.5f, 0.5f);
				}
				
			} else if (color == "ffffff00")	// end
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				endTile = ((Transform)nv);
				((Transform)nv).gameObject.renderer.material = StartTileMaterial;
				lights = ((Transform)nv).gameObject.GetComponentsInChildren<Light>();
    			foreach (Light l in lights)
				{
	        		l.enabled = true;
					l.color = new Color(0.5f, 0.5f, 1f);
				}
				
			} else if (color == "ff00ffff")	// turret
			{
				nv = Instantiate(TurretPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				((Transform)nv).gameObject.GetComponentInChildren<TurretScript>().SetPlayer(Player);
				((Transform)nv).gameObject.GetComponentInChildren<TurretScript>().SetCamera(FPSCamera);
				voxels.Add(nv);
				
			} else if (color == "ffff00ff")	// gun
			{
				nv = Instantiate(GunPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				LEVEL_HAS_GUN = true;
				Debug.Log("level has gun");
				
			} else if (color == "ff6d0092")	// button tile
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((Transform)nv).gameObject.renderer.material = ButtonTileMaterial;
				((Transform)nv).tag = "buttoner";
				lights = ((Transform)nv).gameObject.GetComponentsInChildren<Light>();
	    		foreach (Light l in lights)
				{
        			l.enabled = true;
					l.color = new Color(0.5f, 0.5f, 1f);
				}
				LEVEL_HAS_BUTTON = true;
				BUTTON_PRESSED = false;
				Debug.Log("level has button");
				
			} else if (color == "ff6e6e6e")	// crack tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = CrackedTileMaterial;
				
			} else if (color == "ff929292")	// quad tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = GreyTileQuadMaterial;
				
			} else if (color == "ffb7b7b7")	// double tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = GreyTileDoubleMaterial;
				
			} else if (color == "ff000000")	// help tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = HelpTileMaterial;
				
			} else if (color == "ff800000")	// blood tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = BloodyTileMaterial;
				
			} else if (color == "ffc08e6c") // alt tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = GreyTileAltMaterial;
				
			} else 							// grey tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity);
				voxels.Add(nv);
				((GameObject)nv).renderer.material = GreyTileMaterial;
			}
		}
		sr.Close();
	}
}
