using UnityEngine;
using System.Collections;

public class BlockInfo : MonoBehaviour {

	public string info = "";
	public int type;
	public int layer;
	public int rotation;
	
	public void updateInfo()  {
		info += transform.position.x.ToString() + " ";
		info += transform.position.y.ToString() + " ";
		info += transform.position.z.ToString() + " ";
		info += rotation.ToString()+ " "; 
		info += BlockScript.BlockNames[type] + " ";
		info += layer.ToString();
	}
}
