using UnityEngine;
using System.Collections;

/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)
[AddComponentMenu("Camera-Control/Mouse Look")]
public class CustomMouseLook : MonoBehaviour {

	public float sensitivityX = 15F;
	public float sensitivityY = 15F;
	public bool IsShaking = false;
	public bool IsRockig = false;
	
	public GameObject GunCamera;
	public GameObject BloodFlash;
	
	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;
	
	private static bool invertY = false;

	float rotationY = 0F;
	
	private float hitTimer = 0;
	private const float hitTimeout = 1f;
	private float hitAmount = 20;
	
	public void Hit()
	{
		hitAmount = Random.Range(-10,10);
		BloodFlash.GetComponent<BloodScript>().Hit();
		hitTimer = 0;
	}
	
	void Update ()
	{
		hitTimer += Time.deltaTime;
		int shake = IsShaking?Random.Range(-2, 2):0;
		if (Input.GetKeyDown(KeyCode.I))
			invertY = !invertY;
		// camera movement
		if (IsRockig)
		{
			CharacterController cont = transform.parent.GetComponent<CharacterController>();
			if (cont.isGrounded)
			{
				Vector3 vel = cont.velocity;
				float speed = Mathf.Min(vel.magnitude,4f)/4f;
				transform.parent.audio.volume = speed;
				
				Vector3 pos = transform.localPosition;
				pos.x = Mathf.Sin(Time.time*8.0f)*0.2f*speed;
				pos.y = 0.9f-Mathf.Abs(Mathf.Sin(Time.time*8.0f))*0.3f*speed;
				transform.localPosition = pos;
				
				pos = GunCamera.transform.localPosition;
				pos.x = Mathf.Sin(Time.time*8.0f)*0.01f*speed;
				pos.y = -Mathf.Abs(Mathf.Sin(Time.time*8.0f))*0.01f*speed;
				GunCamera.transform.localPosition = pos;
			}
		}
		if (invertY)
			rotationY -= Input.GetAxis("Mouse Y") * sensitivityY;
		else
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
		rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
		float hitAngle = 0;
		if (hitTimer < hitTimeout)
			hitAngle = hitAmount*(1f-hitTimer/hitTimeout);
		
		transform.localEulerAngles = new Vector3(-rotationY+shake+hitAngle, transform.localEulerAngles.y, hitAngle);
	}
	
	void Start ()
	{
		hitTimer = hitTimeout;
		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;
	}
}