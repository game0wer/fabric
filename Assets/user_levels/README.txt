EDITOR URL;
http://cubekingdom.web.fc2.com/

ABOUT USER LEVELS;
-User levels can be accessed by pressing U in the title screen.
-Level names are in the cko format and are as follows: level#.cko
-They must be placed in the user_levels folder in the game data folder.

MAX WIDTH:32
MAX HEIGHT:16
MAX DEPTH:32

ABOUT BLOCKS;
Blocks start from (100,100,100) and grow in positive directions(e.g blocks at lower coordinates don't get loaded)

BLOCK COLORS(R,G,B);
GUN(255,0,255)	-- if you want the player to start with no gun
NORMAL(255,255,255)
CRACKED(110,110,110)
BLOODY(128,0,0)
SHIFTABLE(0,0,255)
LIGHT(255,0,0)
START(0,255,0)
FINISH(255,255,0)