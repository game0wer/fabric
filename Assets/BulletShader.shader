// Upgrade NOTE: replaced 'PositionFog()' with multiply of UNITY_MATRIX_MVP by position
// Upgrade NOTE: replaced 'V2F_POS_FOG' with 'float4 pos : SV_POSITION'

//
// Shader: "FX/Force Field"
// Version: v1.0
// Written by: Thomas Phillips
//
// Anyone is free to use this shader for non-commercial or commercial projects.
//
// Description:
// Generic force field effect.
// Play with color, opacity, and rate for different effects.
//

Shader "FX/Force Field" {
Properties {

    _Color ("Color Tint", Color) = (1,1,1,1)

    _Rate ("Oscillation Rate", Range (1, 300)) = 300

}
	SubShader {

	    Tags { "Queue" = "Transparent" }
	    Blend One One
    	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	    Pass
	    {
			CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct v2f members fpos)
#pragma exclude_renderers d3d11 xbox360
			// Upgrade NOTE: excluded shader from Xbox360; has structs without semantics (struct v2f members fpos)
			#pragma exclude_renderers xbox360
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_fog_exp2
			#include "UnityCG.cginc"
			float4 _Color;
			float _Rate;
			struct v2f {
			    float4 pos : SV_POSITION;
			    float4 texcoord : TEXCOORD0;
			    float4 fpos;
			};
			v2f vert (appdata_base v)
			{
			    v2f o;
			    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			    o.texcoord = v.texcoord;
				o.fpos.z = v.vertex;
			    return o;
			}
			half4 frag (v2f i) : COLOR
			{
			    float3 color;
			    float m = _Time.x*_Rate;
			    m = sin(i.texcoord.y*10f-m*10.0f);
			    m = m>0.2f?m-0.2f:0f;
			    color = float3(m*_Color.r, m*_Color.g, m*_Color.b);
			    return half4( color, 1 );
			}
			ENDCG
		}
	}
	Fallback "Transparent/Diffuse"
}